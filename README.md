# Overview
This will automatically download all your receipts from Meijer.  It defeats
Akamai's anti-automation technologies to do so, because heaven forbid anyone
be able to get their receipts without having to manually log in and download
them one at a time...

# Dependencies
In Ubuntu, everything is available via apt and pip:

```
apt install -yq --no-install-recommends git python3-pip davfs2 firefox firefox-geckodriver mitmproxy
pip3 install -r requirements.txt
./setup.py build install
```

In Debian, you'll need to get the geckodriver manually:

```
wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
tar zxf geckodriver-v0.30.0-linux64.tar.gz
sudo mv geckodriver /usr/bin/
```

# Quickstart
In one terminal:
```
mitmdump -p 8080 -s "inject.py"
```

In another terminal:
```
export MEIJER_USERNAME="2125551212"
export MEIJER_PASSWORD="hunter2"
# If your locale isn't already set to utf-8, do so now
sudo update-locale LANG=en_US.utf8
./main.py -v
```

# Bugs
If you find any, open an issue on the GitLab issue tracker.

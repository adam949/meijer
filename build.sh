#!/bin/sh -e
# This will intall the dependencies and the meijer library

# Determine if we need to use sudo (docker-based CI jobs run as root)
user=$(whoami)
if [ "$user" != "root" ]; then
	echo "Not running as root, will use sudo"
	sudo="sudo"
else
	echo "Running as root, sudo not needed"
	sudo=""
fi

export DEBIAN_FRONTEND=noninteractive
$sudo apt-get update
$sudo apt-get install -yq --no-install-recommends git python3-pip mitmproxy wget python3-bs4 python3-lxml python3-selenium
$sudo apt-get install -yq firefox || $sudo apt-get install -yq firefox-esr
$sudo apt-get install -yq firefox-geckodriver || $(
  wget https://github.com/mozilla/geckodriver/releases/download/v0.30.0/geckodriver-v0.30.0-linux64.tar.gz
  tar zxf geckodriver-v0.30.0-linux64.tar.gz
  $sudo mv geckodriver /usr/bin/
  rm geckodriver-v0.30.0-linux64.tar.gz
)
$sudo ./setup.py build install

#!/usr/bin/env python3

from distutils.core import setup
from setuptools import find_packages

setup(name='meijer',
      version='1.0',
      description="Library to interface with Meijer's API",
      author='Adam DC949',
      author_email='adam@dc949.org',
      packages=find_packages(".", include=['*.py']),
      url='https://gitlab.com/adam949/meijer',
     )

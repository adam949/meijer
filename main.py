#!/usr/bin/env python3
from argparse import ArgumentParser
from logging import basicConfig, WARNING
from meijer_selenium import Meijer
from os import environ, getcwd, path

LOG_FORMAT = "%(levelname)-8s %(asctime)-15s %(message)s"
DOWNLOAD_DIR = path.join(getcwd(), "receipts")


def handle_arguments():
	"""
	Handle all the arguments, including setting log levels based on the -v/-q
	flags.

	:returns: arguments from the ArgumentParser
	:rtype: `py:argparse.Namespace` object
	"""
	parser = ArgumentParser()
	group = parser.add_mutually_exclusive_group()
	group.add_argument('--verbose', '-v', action='count', default=0)
	group.add_argument('--quiet', '-q', action='count', default=0)
	parser.add_argument('--download-dir', '-d', default=DOWNLOAD_DIR,
		help="Directory to store receipts (Default: %s)" % DOWNLOAD_DIR)
	parser.add_argument('--screenshots', action="store_true",
		help="Take screenshots to aid in debugging")
	args = parser.parse_args()
	basicConfig(format=LOG_FORMAT, level=WARNING+args.quiet*10-args.verbose*10)
	return args

if __name__ == "__main__":
	args = handle_arguments()
	m = Meijer(environ["MEIJER_USERNAME"], environ["MEIJER_PASSWORD"],
			args.download_dir, args.screenshots)
	try:
		m.login()
	except:
		# Give it another chance
		m.login()
	m.get_receipts()
	m.driver.quit()

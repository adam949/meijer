// Pass the User-Agent Test.
const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
  'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
Object.defineProperty(navigator, 'userAgent', {
  get: () => userAgent,
});

// Pass the Webdriver Test.
Object.defineProperty(navigator, 'webdriver', {
  get: () => false,
});

// Pass the Chrome Test.
// We can mock this in as much depth as we need for the test.
window.navigator.chrome = {
  runtime: {
    app: {
      isInstalled: false,
    },
    webstore: {
      onInstallStageChanged: {},
      onDownloadProgress: {},
    },
    runtime: {
      PlatformOs: {
        MAC: 'mac',
        WIN: 'win',
        ANDROID: 'android',
        CROS: 'cros',
        LINUX: 'linux',
        OPENBSD: 'openbsd',
      },
      PlatformArch: {
        ARM: 'arm',
        X86_32: 'x86-32',
        X86_64: 'x86-64',
      },
      PlatformNaclArch: {
        ARM: 'arm',
        X86_32: 'x86-32',
        X86_64: 'x86-64',
      },
      RequestUpdateCheckStatus: {
        THROTTLED: 'throttled',
        NO_UPDATE: 'no_update',
        UPDATE_AVAILABLE: 'update_available',
      },
      OnInstalledReason: {
        INSTALL: 'install',
        UPDATE: 'update',
        CHROME_UPDATE: 'chrome_update',
        SHARED_MODULE_UPDATE: 'shared_module_update',
      },
      OnRestartRequiredReason: {
        APP_UPDATE: 'app_update',
        OS_UPDATE: 'os_update',
        PERIODIC: 'periodic',
      },
    },
  }
};
window.chrome = window.navigator.chrome

// Pass the Permissions Test.
const originalQuery = window.navigator.permissions.query;
window.navigator.permissions.query = (parameters) => (
  parameters.name === 'notifications' ?
    Promise.resolve({ state: Notification.permission }) :
    originalQuery(parameters)
);

// Pass the Plugins Length Test.
// Overwrite the `plugins` property to use a custom getter.
Object.defineProperty(navigator, 'plugins', {
  // This just needs to have `length > 0` for the current test,
  // but we could mock the plugins too if necessary.
  get: () => [1, 2, 3, 4, 5],
});

// Pass the Languages Test.
// Overwrite the `plugins` property to use a custom getter.
Object.defineProperty(navigator, 'languages', {
  get: () => ['en-US', 'en'],
});

// Pass the screen resolution Test.
/*
Object.defineProperty(window, 'screen', {
  get: () => { availWidth: 1824, availHeight: 1185, width: 1824, height: 1216, colorDepth: 24, pixelDepth: 24, top: 0, left: 0, availTop: 0, availLeft: 0 },
});
*/


/**

// Here's the big fuck you to anyone who wants to access anything on window without being caught!
// navigator
navigator._appCodeName = navigator.appCodeName
Object.defineProperty(navigator, "appCodeName", { get: function() { console.log("Access to navigator.appCodeName detected"); return this["_appCodeName"]; } })

navigator._appName = navigator.appName
Object.defineProperty(navigator, "appName", { get: function() { console.log("Access to navigator.appName detected"); return this["_appName"]; } })

navigator._appVersion = navigator.appVersion
Object.defineProperty(navigator, "appVersion", { get: function() { console.log("Access to navigator.appVersion detected"); return this["_appVersion"]; } })

navigator._buildID = navigator.buildID
Object.defineProperty(navigator, "buildID", { get: function() { console.log("Access to navigator.buildID detected"); return this["_buildID"]; } })

navigator._clipboard = navigator.clipboard
Object.defineProperty(navigator, "clipboard", { get: function() { console.log("Access to navigator.clipboard detected"); return this["_clipboard"]; } })

navigator._cookieEnabled = navigator.cookieEnabled
Object.defineProperty(navigator, "cookieEnabled", { get: function() { console.log("Access to navigator.cookieEnabled detected"); return this["_cookieEnabled"]; } })

navigator._credentials = navigator.credentials
Object.defineProperty(navigator, "credentials", { get: function() { console.log("Access to navigator.credentials detected"); return this["_credentials"]; } })

navigator._doNotTrack = navigator.doNotTrack
Object.defineProperty(navigator, "doNotTrack", { get: function() { console.log("Access to navigator.doNotTrack detected"); return this["_doNotTrack"]; } })

navigator._geolocation = navigator.geolocation
Object.defineProperty(navigator, "geolocation", { get: function() { console.log("Access to navigator.geolocation detected"); return this["_geolocation"]; } })

navigator._hardwareConcurrency = navigator.hardwareConcurrency
Object.defineProperty(navigator, "hardwareConcurrency", { get: function() { console.log("Access to navigator.hardwareConcurrency detected"); return this["_hardwareConcurrency"]; } })

navigator._language = navigator.language
Object.defineProperty(navigator, "language", { get: function() { console.log("Access to navigator.language detected"); return this["_language"]; } })

navigator._maxTouchPoints = navigator.maxTouchPoints
Object.defineProperty(navigator, "maxTouchPoints", { get: function() { console.log("Access to navigator.maxTouchPoints detected"); return this["_maxTouchPoints"]; } })

navigator._mediaDevices = navigator.mediaDevices
Object.defineProperty(navigator, "mediaDevices", { get: function() { console.log("Access to navigator.mediaDevices detected"); return this["_mediaDevices"]; } })

navigator._mediaSession = navigator.mediaSession
Object.defineProperty(navigator, "mediaSession", { get: function() { console.log("Access to navigator.mediaSession detected"); return this["_mediaSession"]; } })

navigator._permissions = navigator.permissions
Object.defineProperty(navigator, "permissions", { get: function() { console.log("Access to navigator.permissions detected"); return this["_permissions"]; } })

navigator._mimeTypes = navigator.mimeTypes
Object.defineProperty(navigator, "mimeTypes", { get: function() { console.log("Access to navigator.mimeTypes detected"); return this["_mimeTypes"]; } })

navigator._mediaCapabilities = navigator.mediaCapabilities
Object.defineProperty(navigator, "mediaCapabilities", { get: function() { console.log("Access to navigator.mediaCapabilities detected"); return this["_mediaCapabilities"]; } })

navigator._onLine = navigator.onLine
Object.defineProperty(navigator, "onLine", { get: function() { console.log("Access to navigator.onLine detected"); return this["_onLine"]; } })

navigator._oscpu = navigator.oscpu
Object.defineProperty(navigator, "oscpu", { get: function() { console.log("Access to navigator.oscpu detected"); return this["_oscpu"]; } })

navigator._platform = navigator.platform
Object.defineProperty(navigator, "platform", { get: function() { console.log("Access to navigator.platform detected"); return this["_platform"]; } })

navigator._product = navigator.product
Object.defineProperty(navigator, "product", { get: function() { console.log("Access to navigator.product detected"); return this["_product"]; } })

navigator._productSub = navigator.productSub
Object.defineProperty(navigator, "productSub", { get: function() { console.log("Access to navigator.productSub detected"); return this["_productSub"]; } })

navigator._serviceWorker = navigator.serviceWorker
Object.defineProperty(navigator, "serviceWorker", { get: function() { console.log("Access to navigator.serviceWorker detected"); return this["_serviceWorker"]; } })

navigator._storage = navigator.storage
Object.defineProperty(navigator, "storage", { get: function() { console.log("Access to navigator.storage detected"); return this["_storage"]; } })

navigator._vendor = navigator.vendor
Object.defineProperty(navigator, "vendor", { get: function() { console.log("Access to navigator.vendor detected"); return this["_vendor"]; } })

navigator._vendorSub = navigator.vendorSub
Object.defineProperty(navigator, "vendorSub", { get: function() { console.log("Access to navigator.vendorSub detected"); return this["_vendorSub"]; } })



// window
window._close = window.close
Object.defineProperty(window, "close", { get: function() { console.log("Access to window.close detected"); return this["_close"]; } })
//...
window._navigator = window.navigator
Object.defineProperty(window, "navigator", { get: function() { console.log("Access to window.navigator detected"); return this["_navigator"]; } })
window._chrome = window.chrome
Object.defineProperty(window, "chrome", { get: function() { console.log("Access to window.chrome detected"); return this["_chrome"]; } })


// */


// These are non-configurable properties:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cant_redefine_property
/*
// navigator
navigator._languages = navigator.languages
Object.defineProperty(navigator, "languages", { get: function() { console.log("Access to navigator.languages detected"); return this["_languages"]; } })
navigator._userAgent = navigator.userAgent
Object.defineProperty(navigator, "userAgent", { get: function() { console.log("Access to navigator.userAgent detected"); return this["_userAgent"]; } })
navigator._plugins = navigator.plugins
Object.defineProperty(navigator, "plugins", { get: function() { console.log("Access to navigator.plugins detected"); return this["_plugins"]; } })
navigator._webdriver = navigator.webdriver
Object.defineProperty(navigator, "webdriver", { get: function() { console.log("Access to navigator.webdriver detected"); return this["_webdriver"]; } })


// window
window._document = window.document
Object.defineProperty(window, "document", { get: function() { console.log("Access to window.document detected"); return this["_document"]; } })

*/

// e = HTMLDocument = window.document.documentElement
// t = ?
// n = ?
// 
// mouseover { target: nav.navbar.GlobalHeader__nav.align-items-center, buttons: 0, clientX: 1061, clientY: 59, layerX: 1020, layerY: 24 }
// is n(183) == nav.navbar.GlobalHeader__nav?

// e = event string (e.g. "focus")
// t = 1
// n = HTMLDocument
// r = object (e.g. focus { target: HTMLDocument https://www.meijer.com/, isTrusted: true, view: Window, detail: 0, layerX: 0, layerY: 0, which: 0, rangeOffset: 0, SCROLL_PAGE_UP: -32768, SCROLL_PAGE_DOWN: 32768, … })


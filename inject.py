#!/usr/bin/env python3
from bs4 import BeautifulSoup
from mitmproxy import ctx
from pprint import pformat
from time import time
from urllib.parse import parse_qs, unquote

# DEBUG mode will dump all POST data to disk
DEBUG=False

# load in the javascript to inject
with open('content.js', 'r') as f:
	content_js = f.read()

def request(flow):
	# Spy on the requests so we can see what the headless browser sends
	if flow.request.method == "POST":
		c = unquote(flow.request.content.decode())
		ctx.log.info("POST to {}: {}".format(flow.request.path, c))
		if DEBUG:
			qs_object = parse_qs(flow.request.content.decode())
			if qs_object:  # If it's not empty
				filename = "{}.postdata".format(time())
				with open(filename, 'w') as f:
					f.write("{}\n".format(flow.request.path))
					f.write("{}\n".format(pformat(qs_object)))
def response(flow):
	# only process 200 responses of html content
	if "Content-Type" not in flow.response.headers:
		ctx.log.debug("Not injecting because Content-Type response header was missing")
		return
	if not flow.response.headers['Content-Type'].startswith('text/html'):
		ctx.log.debug("Not injecting because Content-Type was {}".format(flow.response.headers['Content-Type']))
		return
	if not flow.response.status_code == 200:
		ctx.log.debug("Not injecting because status code was {}".format(flow.response.status_code))
		return 
  
	# inject the script tag
	html = BeautifulSoup(flow.response.text, 'lxml')
	container = html.head or html.body
	if container:
		script = html.new_tag('script', type='text/javascript')
		with open('content.js', 'r') as f:
			script.string = f.read()
		#script.string = content_js
		container.insert(0, script)
		flow.response.text = str(html)
		ctx.log.info('Successfully injected the content.js script.')

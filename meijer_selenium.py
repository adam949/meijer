#!/usr/bin/env python3
from datetime import datetime
from logging import debug, info, warning, error, critical
from os import getcwd, mkdir, path, rename
from selenium.webdriver import ActionChains
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.remote.webdriver import By
from time import sleep, time

class Meijer():
	FRONT_PAGE = "https://www.meijer.com/"
	RECEIPTS_URL = "https://www.meijer.com/mperks/receipts-savings.html"

	def __init__(self, username="", password="", download_directory=None, debug=False):
		"""
		Creates the Meijer interface.

		:param username: Username (phone/email) to use during login
		:type username: str
		:param password: Password to use during login
		:type password: str
		:param download_directory: The full path to the directory where
			files should be saved
		:type download_directory: str
		:param debug: Flag to enable taking screenshots after page loads
		:type debug: bool
		"""
		self.username = username
		self.password = password
		if download_directory is None:
			self.DOWNLOAD_DIRECTORY = path.join(getcwd(), "receipts")
		else:
			if download_directory[0] == '/':
				# If we have an absolute path, take it as is
				self.DOWNLOAD_DIRECTORY = download_directory
			else:
				# Otherwise prepend the cwd
				self.DOWNLOAD_DIRECTORY = path.join(getcwd(), download_directory)
		info("Download directory = {}".format(self.DOWNLOAD_DIRECTORY))
		if not path.exists(self.DOWNLOAD_DIRECTORY):
			mkdir(self.DOWNLOAD_DIRECTORY)
		self.debug = debug
		options = Options()
		options.add_argument("--headless")
		options.set_preference('dom.webdriver.enabled', False)
		options.set_preference("network.proxy.type", 1)
		options.set_preference("network.proxy.http", "localhost")
		options.set_preference("network.proxy.http_port", 8080)
		options.set_preference("network.proxy.share_proxy_settings", True)
		options.set_preference("network.proxy.ssl", "localhost")
		options.set_preference("network.proxy.ssl_port", 8080)
		options.accept_insecure_certs = True  # b/c we're MitMing everything
		# Setting to download PDFs instead of viewing them taken from
		# https://stackoverflow.com/questions/23800195/auto-download-pdf-in-firefox
		# Also see http://kb.mozillazine.org/About:config_entries#Browser.
		options.set_preference("pdfjs.disabled", True)
		options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
		options.set_preference("browser.download.folderList", 2);
		options.set_preference("browser.download.dir", self.DOWNLOAD_DIRECTORY)
		self.driver = WebDriver(options=options)
		# We have to wait after each action so the JS on the page has
		# a chance to update the page elements.
		self.driver.implicitly_wait(7)  # 7 seconds

	def login(self):
		"""
		Attempts to log in using the object's username and password.
		After this is successful, the web session will have all the
		cookies to perform authenticated requests.
		"""
		# Start at the front page, then click buttons to get to the login page
		self.driver.get(self.FRONT_PAGE)
		debug("Clicking the user icon")
		self.driver.find_element(By.CLASS_NAME, "meijer-header__account-signin-button").click()
		debug("Clicking the sign in link")
		self.driver.find_element(By.XPATH, "//button[text()='Sign In']").click()
		sleep(15)
		if self.debug:
			self.driver.get_full_page_screenshot_as_file('{}.png'.format(time()))

		# Enter username and click Next
		debug("Locating the username field")
		self.driver.find_element(By.ID, "input27").send_keys(self.username)

		# Enter password and click Sign In
		debug("Locating the password field")
		self.driver.find_element(By.ID, "input35").send_keys(self.password)
		# We need to use Actions in order to trigger the events such as
		# onmouseover, which are used to detect bots
		actions = ActionChains(self.driver)
		actions.move_to_element(self.driver.find_element(By.CLASS_NAME, "button-primary"))
		actions.click()
		debug("Clicking the sign in button")
		actions.perform()
		sleep(15)
		if self.debug:
			self.driver.get_full_page_screenshot_as_file('{}.png'.format(time()))

	def get_receipts(self):
		"""
		Downloads all receipts for the currently logged in user.
		"""
		self.driver.get(self.RECEIPTS_URL)
		sleep(15)  # Let the javascript do its thing and fill out the table
		if self.debug:
			self.driver.get_full_page_screenshot_as_file('{}.png'.format(time()))
		chatbot = self.driver.find_element(By.ID, "new-chatbot")
		if chatbot:
			# The chatbot can mess up our ability to click download,
			# so we remove that element if it's there
			s = "var e = arguments[0];e.parentNode.removeChild(e);"
			self.driver.execute_script(s, chatbot)
		table = self.driver.find_element(By.TAG_NAME, "table")
		rows = table.find_elements(By.TAG_NAME, "tr")
		for row in rows:
			cells = row.find_elements(By.TAG_NAME, "td")
			if len(cells) <= 0:
				continue
			# Determine the filename and skip it if we already have this file
			d = datetime.strptime(cells[0].get_attribute("innerText"), '%m/%d/%Y')
			items = cells[2].get_attribute("innerText")
			if items == "0":
				info("{} is an HTML receipt for an online order, not a PDF, skipping download".format(dest_name))
				continue
			dest_name = "{}_{}.pdf".format(d.strftime("%Y-%m-%d"), items)
			if path.exists(path.join(self.DOWNLOAD_DIRECTORY, dest_name)):
				debug("{} already exists, skipping download".format(dest_name))
				continue
			# Click the link!
			try:
				image = row.find_element(By.TAG_NAME, "img")
				# Clicking an image for a receipt which is
				# unavailable not only doesn't work, but it also
				# results in a modal popup that will prevent any
				# other receipts from being downloaded, so we
				# want to avoid this situation
				if "pdf-unavailable" in image.get_attribute("src"):
					info("{} is unavailable".format(dest_name))
					continue
				image.click()
				info("Downloading receipt {}".format(dest_name))
				# Wait for tab to launch, mime type to come back, and file to download()
				sleep(10)
				# Rename to date/items so we don't have 100+ files with the same name
				rename(path.join(self.DOWNLOAD_DIRECTORY, "meijer_digital_receipt.pdf"),
					path.join(self.DOWNLOAD_DIRECTORY, dest_name))
			except Exception as e:
				# If, for whatever reason, there's no image, we just move along
				warning("Unable to download receipt: {}".format(e))
		
if __name__ == "__main__":
	from os import environ
	m = Meijer(environ["MEIJER_USERNAME"], environ["MEIJER_PASSWORD"])
	try:
		m.login()
	except:
		# Give it another chance
		m.login()
	m.get_receipts()
	m.driver.quit()

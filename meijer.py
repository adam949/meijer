#!/usr/bin/env python3
from bs4 import BeautifulSoup
from json import dumps, loads
from os import environ
from requests import Session

class Meijer():
	AUTH_URL = "https://login.meijer.com/as/authorization.oauth2?client_id=digital&response_type=code&scope=openid&pfidpadapterid=accounts&state=https://www.meijer.com/&redirect_uri=https://www.meijer.com/bin/meijer/signin/callback"
	CALLBACK_RESUME_URL = "https://accounts.meijer.com/manage/Credentials/CallbackResume"
	VALIDATE_USER_URL = "https://accounts.meijer.com/manage/Credentials/ValidateUserInformation"
	LOGIN_URL = "https://accounts.meijer.com/manage/Credentials/MeijerLogin"
	RECEIPTS_URL = "https://www.meijer.com/bin/meijer/receipts"
	headers = {
		"User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0",
		"Accept": "application/json, text/plain, */*",
		"Accept-Language": "en-US,en;q=0.5",
		"Accept-Encoding": "gzip, deflate, br",
		"X-Requested-With": "XMLHttpRequest",
		"Content-Type": "application/json;charset=utf-8",
		"Origin": "https://accounts.meijer.com",
		"DNT": "1",
		"Referer": "https://accounts.meijer.com/manage/Account",
		"Sec-GPC": "1",
		"TE": "Trailers"
	}

	def __init__(self, username="", password=""):
		self.username = username
		self.password = password
		self.s = Session()

	def login(self):
		"""
		Attempts to log in using the object's username and password.
		After this is successful, the web session will have all the
		cookies to perform authenticated requests.
		"""
		# Stage one, get cookies
		r = self.s.get(self.AUTH_URL)
		bs = BeautifulSoup(r.text, "html.parser")
		path = bs.find("input", attrs={"name": "resumePath"})['value']
		r = self.s.get(self.CALLBACK_RESUME_URL,
				params={"allowInteraction": "True", "resumePath": path, "reauth": "False"},
				headers=self.headers)

		# Stage two, enter username
		r = self.s.post(self.VALIDATE_USER_URL, data=dumps({"userData":self.username}),
				headers=self.headers)
		j = loads(r.text)

		# Stage three, enter username and password
		postdata = {"autoPopEmail":"",
				"email":j["Data"]["Email"],
				"ErrorMessage":"",
				"hideEmail":j["Data"]["Email"],
				"isAndroidPaySignInFlow":"",
				"password":self.password,
				"pin":"",
				"rememberMe":"true",
				"showSuccess":False,
				"topSuccessMessage":"",
				"userPhoneNumber":j["Data"]["Phone"]}
		r = self.s.post(self.LOGIN_URL, data=dumps(postdata), headers=self.headers)
		j = loads(r.text)

		# Stage four, go to callback to get more cookies
		r = self.s.get(j["RedirectUrl"], headers=self.headers)

	def get_receipts(self):
		"""
		Obtains a list of receipts for the currently logged in user.
		"""
		postdata = {"fromDate":"2000-01-01",
				"toDate":"2037-12-31T23:59:59",
				"numberOfReceiptsInEachPage":20,
				"pageIndex":0,
				"type":1}
		r = self.s.post(self.RECEIPTS_URL, data=dumps(postdata), headers=self.headers)
		return loads(r.text)

	def get_receipt(self, receipt_id):
		"""
		Obtains the URL for a receipt, given its ID

		@param receipt_id The ID of the receipt you're interested in
		@type receipt_id string
		"""
		postdata = {"receiptId":receipt_id,"formatType":"PDF","type":2}
		r = self.s.post(self.RECEIPTS_URL, data=dumps(postdata), headers=self.headers)
		return r.text

if __name__ == "__main__":
	m = Meijer(environ["MEIJER_USERNAME"], environ["MEIJER_PASSWORD"])
	m.login()
	j = m.get_receipts()
	for r in j["receipts"]:
		print(m.get_receipt(r['receiptId']))
